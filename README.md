github开源项目，mybatis generator,就是mybatis的逆向工具，url:https://github.com/mybatis/generator/releases
使用说明：
访问url,不要去clone项目，而是下载压缩包mybatis-generator-core-1.3.7.zip，
然后解压找到pom.xml,获取groupId,artifactId,然后mvn install,这样的好处就是可以mvn package,
如果把其作为外挂包，那么mvn package会报错。
接着是配置文件的使用，文件中生成实体类，dao和mapper的路径指定为生成的jar包同级目录下的src/main/java下，且这3级目录需要手动创建，否则不会生成实体类,dao,mapper
需要连接哪种数据库，就需要maven引入对应的驱动依赖，目前工程支持mysql和sql server