package com.damon;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GenerateEntityDaoMapper {

    public void generator() throws Exception {
        System.out.println("entered generator()");
        //获取系统路径是斜杠 / 还是反斜杠 \
        String fileSeparator = System.getProperty("file.separator");
        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
        //配置文件的相对路径，和工程处于同级目录
        String configFilePath = "." + fileSeparator + "generatorConfig.xml";
        // 指定配置文件
        File configFile = new File(configFilePath);
        System.out.println("config file path=" + configFile.getAbsolutePath());
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);
        System.out.println("generator() finished");
    }

    // 执行main方法以生成代码
    public static void main(String[] args) {
        System.out.println("main running..");
        try {
            GenerateEntityDaoMapper generatorSqlmap = new GenerateEntityDaoMapper();
            generatorSqlmap.generator();
            System.out.println("main finished");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
